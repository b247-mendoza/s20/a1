// Activity 1
let num = Number(prompt('Enter a number'));

	for (let n = num; n >= 0; n--) {
		console.log('The number you provided is ' + n);
		if (n <= 50) {
			break;
		};

		if (n % 10 === 0){
			console.log('The number is divisible by 10. Skipping the number.');
			continue;
		};

		if (n % 5 === 0) {
			console.log(n);
		};
		
		break;
	};

	// Activity 2
	let variableString = 'supercalifragilisticexpialidocious';
	let consonant="";
	
	for (let i = 0; i < variableString.length; i++){
		if (variableString[i] =='a' || variableString[i] =='e' || variableString[i] =='i' || variableString[i] =='o' || variableString[i] =='u') {
			continue;
		} else {
			consonant += variableString[i];
		}

	};

	console.log(consonant);
		